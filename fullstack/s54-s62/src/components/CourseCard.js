import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState } from 'react';

export default function CourseCard({course}){

	// Destructuring the contents of 'course'
	const {name, description, price} = course;

	// A state is just like a variable but with the concept of getters and setters. Th getter us responsible for retrieving the current value of the state, while the setter is responsible for modifying the current value of the state. The useState() hook is responsible for setting the initial value of the state.
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll(){
		if (seats > 0) {
			// The setCount function can use the previous value of the state and add/modify to it.
			setCount(prev_value => prev_value + 1);
			setSeats(prev_seats => prev_seats -1 );
		} else {
			alert('No more seats available');
		}
	}

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
			

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP{price}</Card.Text>


				<Card.Subtitle>Enrollees:</Card.Subtitle>
				<Card.Text>{count}</Card.Text>

				<Card.Subtitle>Seats:</Card.Subtitle>
				<Card.Text>{seats}</Card.Text>

			  	<Button variant="primary" onClick={enroll}>Enroll</Button>
			  </Card.Body>
		</Card>
	)
}

// PropTypes is used for validating the data from the props
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}