import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Bootstrap import
import 'bootstrap/dist/css/bootstrap.min.css';

// This is where React attaches the component to the root element in the index.html file
// StrictMode allows react to be able to display and handle any error/warning 
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

