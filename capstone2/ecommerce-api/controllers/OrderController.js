const Order = require("../models/Order.js");
// const User = require("../models/User.js");
// const Product = require("../models/Product");

module.exports.createOrder = (request_body) => {
  let totalAmount = 0;
  for (const product of products) {
  }
  let new_order = new Order({
    userId: request_body.userId,
    products: request_body.products,
    totalAmount: request_body.totalAmount,
  });

  return new_order
    .save()
    .then((created_order, error) => {
      if (error) {
        return {
          message: error.message,
        };
      }

      return {
        message: "Order created successfully!",
      };
    })
    .catch((error) => {
      return {
        message: error.message,
      };
    });
};

// get all orders
module.exports.getOrders = (request, response) => {
  return Order.find().then((orders) => {
    return response.send(orders);
  });
};

// Get a specific order by ID
module.exports.getOrderById = (request, response) => {
  return Order.findById(request.params.id).then((order_id) => {
    return response.send(order_id);
  });
};

// Update an order by ID
module.exports.updateOrder = (request, response) => {
  let updated_data = {
    productId: request.body.productId,
    quantity: request.body.quantity,
  };
  return Order.findByIdAndUpdate(request.params.id, updated_data).then(
    (order, error) => {
      if (error) {
        return response.send({
          message: error.message,
        });
      }
      return response.send({
        message: "Order has been updated successfully!",
      });
    }
  );
};

// Delete an order by ID
module.exports.deleteOrder = (request, response) => {
  return Order.findByIdAndDelete(request.params.id).then((result, error) => {
    if (error) {
      return response.send({
        message: error.message,
      });
    }
    return response.send({
      message: "Product has been deleted!",
    });
  });
};
