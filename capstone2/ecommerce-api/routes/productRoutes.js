const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/ProductController.js");
const auth = require("../auth.js");

// Create a product
router.post("/", auth.verify, auth.verifyAdmin, (request, response) => {
  ProductController.createProduct(request.body).then((result) => {
    response.send(result);
  });
});

// Get all products
router.get("/all", (request, response) => {
  ProductController.getAllProducts(request, response);
});

// Get all active products
router.get("/", (request, response) => {
  ProductController.getAllActiveProducts(request, response);
});

// Get single product
router.get("/:id", (request, response) => {
  ProductController.getProduct(request, response);
});

// Update a product
router.put("/:id", auth.verify, auth.verifyAdmin, (request, response) => {
  ProductController.updateProduct(request, response);
});

// Archive single product
router.put("/:id/archive", auth.verify, (request, response) => {
  ProductController.archiveProduct(request, response);
});

// Activate product
router.put(
  "/:id/activate",
  auth.verify,
  auth.verifyAdmin,
  (request, response) => {
    ProductController.activateProduct(request, response);
  }
);

// Search product by name
router.post("/search", (request, response) => {
  ProductController.searchProducts(request, response);
});

module.exports = router;
